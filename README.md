# JcBaza API PHP SDK

PHP SDK для JcBaza API

## Использование

```php
$client = new JcbazaClient('username', 'token');
```

## Методы

### Поиск

```php
/** @var SynonymsResponse $response */
$response = $client->sendSynonymsRequest(new SynonymsRequest('Влад'));

$count = $response->getCount(); // 3
$items = $response->getItems(); // ['Владивоcток', 'Владикавказ', 'Владимир']
```

### Поиск с категорией
```php
$idCategory = 3;

/** @var SynonymsResponse $response */
$response = $client->sendSynonymsRequest(new SynonymsRequest('Dkfl', $idCategory));

$count = $response->getCount(); // 3
$items = $response->getItems(); // ['Владивоcток', 'Владикавказ', 'Владимир']
```
