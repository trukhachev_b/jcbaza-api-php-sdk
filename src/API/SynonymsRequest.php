<?php

namespace JustCommunication\JcbazaSDK\API;

class SynonymsRequest extends AbstractRequest
{
    const URI = '/api/v1/service/synonyms';
    const HTTP_METHOD = 'GET';
    const RESPONSE_CLASS = SynonymsResponse::class;

    /**
     * @var string
     */
    protected $string;

    /**
     * @var int
     */
    protected $idCategory;

    /**
     * SynonymsRequest constructor.
     *
     * @param string $string
     */
    public function __construct($string, $idCategory = 0)
    {
        $this->string = $string;
        $this->idCategory = $idCategory;
    }
    /**
     * @inheritDoc
     */
    public function createHttpClientParams()
    {
        return [
            'query' => [
                'synonym' => $this->string,
                'id_category' => $this->idCategory
            ]
        ];
    }
}
