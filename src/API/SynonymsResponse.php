<?php

namespace JustCommunication\JcbazaSDK\API;

class SynonymsResponse extends AbstractResponse
{
    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @inheritDoc
     */
    public function setResponseData(array $responseData)
    {
        if (!empty($responseData['count'])) {
            $this->count = (int)$responseData['count'];
        }

        if (!empty($responseData['data'])) {
            $this->items = $responseData['data'];
        }

        parent::setResponseData($responseData);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }
}
