<?php

namespace JustCommunication\JcbazaSDK\API;

abstract class AbstractResponse implements ResponseInterface
{
    protected $responseData;

    /**
     * @inheritDoc
     */
    public function setResponseData(array $responseData)
    {
        $this->responseData = $responseData;
    }
}
