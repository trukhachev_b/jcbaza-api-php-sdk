<?php

namespace JustCommunication\JcbazaSDK\API;

interface ResponseInterface
{
    /**
     * @param array $responseData
     * @return void
     *
     * @throws JcbazaAPIException
     */
    public function setResponseData(array $responseData);
}
