<?php

namespace JustCommunication\JcbazaSDK;

use GuzzleHttp\Psr7\Response;
use JustCommunication\JcbazaSDK\API\RequestInterface;
use JustCommunication\JcbazaSDK\API\ResponseInterface;
use JustCommunication\JcbazaSDK\Exception\JcbazaAPIException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class JcbazaClient
 *
 * @package JcbazaSDK
 *
 */
class JcbazaClient implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    const API_ENDPOINT = 'https://jcbaza.ru/';

    const DEFAULT_HTTP_CLIENT_OPTIONS = [
        'connect_timeout' => 4,
        'timeout' => 10
    ];

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * JcbazaClient constructor.
     *
     * @param string $username
     * @param string $token
     * @param array|\GuzzleHttp\Client $httpClientOrOptions
     */
    public function __construct($username, $token, $httpClientOrOptions = self::DEFAULT_HTTP_CLIENT_OPTIONS)
    {
        $this->username = $username;
        $this->token = $token;

        $this->httpClient = self::createHttpClient($httpClientOrOptions);

        $this->logger = new NullLogger();
    }

    /**
     * @param array $httpClientOrOptions
     * @return \GuzzleHttp\Client
     */
    protected static function createHttpClient($httpClientOrOptions = self::DEFAULT_HTTP_CLIENT_OPTIONS)
    {
        if (is_array($httpClientOrOptions)) {
            $httpClient = new \GuzzleHttp\Client($httpClientOrOptions);
        } else {
            $httpClient = $httpClientOrOptions;
        }

        return $httpClient;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param \GuzzleHttp\Client $httpClient
     *
     * @return $this
     */
    public function setHttpClient(\GuzzleHttp\Client $httpClient)
    {
        $this->httpClient = $httpClient;
        return $this;
    }

    public function __call($name, array $arguments)
    {
        if (0 === \strpos($name, 'send')) {
            return call_user_func_array([$this, 'sendRequest'], $arguments);
        }

        throw new \BadMethodCallException(\sprintf('Method [%s] not found in [%s].', $name, __CLASS__));
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     *
     * @throws JcbazaAPIException
     */
    public function sendRequest(RequestInterface $request)
    {
        try {
            /** @var Response $response */
            $response = $this->createAPIRequestPromise($request)->wait();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            self::handleErrorResponse($e->getResponse(), $this->logger);

            throw new JcbazaAPIException('JcBaza API Request error: ' . $e->getMessage());
        }

        return self::createAPIResponse($response, $request->getResponseClass());
    }

    public function createAPIRequestPromise(RequestInterface $request)
    {
        $request_params = $request->createHttpClientParams();

        $this->logger->debug('JcBaza API request {method} {uri}', [
            'method' => $request->getHttpMethod(),
            'uri' => $request->getUri(),
            'request_params' => $request_params
        ]);

        $request_params = array_merge_recursive($request_params, [
            'headers' => [
                'X-WSSE' => $this->generateWsseHeader()
            ]
        ]);

        if (!isset($request_params['base_uri'])) {
            $request_params['base_uri'] = self::API_ENDPOINT;
        }

        return $this->httpClient->requestAsync($request->getHttpMethod(), $request->getUri(), $request_params);
    }

    protected function generateWsseHeader()
    {
        $nonce = hash('sha512', uniqid(true));
        $created = date('c');
        $digest = base64_encode(sha1($nonce . $created . $this->token, true));

        return sprintf('UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',
            $this->username,
            $digest,
            $nonce,
            $created
        );
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param string $apiResponseClass
     *
     * @return ResponseInterface
     *
     * @throws JcbazaAPIException
     */
    protected static function createAPIResponse(\Psr\Http\Message\ResponseInterface $response, $apiResponseClass)
    {
        $response_string = (string)$response->getBody();
        $response_data = json_decode($response_string, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JcbazaAPIException('Invalid response data');
        }

        if (isset($response_data['code'], $response_data['message'])) {
            throw new JcbazaAPIException('JcBaza API Error: ' . $response_data['message'], $response_data['code']);
        }

        /** @var ResponseInterface $response */
        $response = new $apiResponseClass;

        if (!$response instanceof ResponseInterface) {
            throw new JcbazaAPIException('Invalid response class');
        }

        $response->setResponseData($response_data);

        return $response;
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface|null $response
     * @throws JcbazaAPIException
     */
    protected static function handleErrorResponse(\Psr\Http\Message\ResponseInterface $response = null, \Psr\Log\LoggerInterface $logger = null)
    {
        if (!$response) {
            return;
        }

        $response_string = (string)$response->getBody();
        $response_data = json_decode($response_string, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JcbazaAPIException('Unable to decode error response data. Error: ' . json_last_error_msg());
        }

        if (isset($response_data['code'], $response_data['message'])) {
            throw new JcbazaAPIException('JcBaza API Error: ' . $response_data['message'], $response_data['code']);
        }
    }

}
